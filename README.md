LV2-World
=========

LV2-World is a [collection][1] of [LV2][2] plugins.

----

LV2-World plugins can be built on top of the [freedesktop-sdk][3] using
[BuildStream][4]:

```sh
    bst build world.bst
```

----

LV2-World plugins can also be built using host-tool with [JHBuild][5]:


```sh
    meson build .
    jhbuild --file=build/jhbuildrc build world
```

----

LV2-World's build recipes as well as its wiki content are licensed under
the MIT License as published by the Open Source Initiative.

[1]: https://gitlab.com/LV2_World/lv2-world/wikis/plugins
[2]: http://lv2plug.in
[3]: https://gitlab.com/freedesktop-sdk/freedesktop-sdk
[4]: https://gitlab.com/BuildStream/buildstream
[5]: https://gitlab.gnome.org/GNOME/jhbuild
